package uf2

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.isNotNull
import org.jetbrains.exposed.sql.transactions.transaction
import java.util.Scanner


object Teams: Table() {
    val name: Column<String> = varchar("name",20)
    val city = varchar("city",20).nullable()
    val conference = varchar("conference",4).nullable()
    val division  = varchar("division",9).nullable()
    override val primaryKey = PrimaryKey(name, name = "pk_team")
}

object Players: Table() {
    val id: Column<Int> = integer("id")
    val name = varchar("name",30).nullable()
    val origin = varchar("origin",20).nullable()
    val height = varchar("height",4).nullable()
    val weight = integer("weight").nullable()
    val position = varchar("position",12).nullable()
    val nameIteam: Column<String> = varchar("nameiteam",20).references(Teams.name)
    override val primaryKey = PrimaryKey(id, name = "pkjug")
}

data class Team(val name: String, val city: String, val conference: String, val division: String)

data class Player(
    val id: Int,
    val name: String,
    val origin: String,
    val height: String,
    val weight: String,
    val position: String,
    val nameIteam: String
    )


fun main() {

    Database.connect("jdbc:postgresql://localhost:5432/nba", driver = "org.postgresql.Driver", user = "sjo")

    transaction {
        addLogger(StdOutSqlLogger)
    }

    //En nom dels diferents equips (en una sola línia)
    transaction {
        Teams.slice(Teams.name).selectAll().forEach { print("${it[Teams.name]} ") }
    }
    //L'usuari introdueix un nom d'un jugador, i printa la ciutat on juga
    val scanner = Scanner(System.`in`)
    println("\nIngrese jugador: ")
    val inputPlayerNom = scanner.nextLine()
    transaction {
        //select city from teams join players p on teams.name = p.nameiteam and p.name = 'Corey Brever';
        val city = (Teams fullJoin Players).slice(Teams.city).select { Teams.name eq Players.nameIteam and (Players.name eq inputPlayerNom) }.map { it.toString() }.firstOrNull()
        println(city) //como ver solamente el valor?
        //El jugador (nom i alçada) més alt de cada equip de la divisió Atlantic
        (Teams fullJoin Players).select { Teams.division eq "Atlantic" }
            .sortedByDescending { it[Players.height] }
            .distinctBy { it[Players.nameIteam] }
            .forEach { println("${it[Players.name]} - ${it[Players.height]}") }
        //L'index de massa corporal mitjana de tots els jugadors peso/(altura al cuadrado)
        val listWeigth = mutableListOf<Double?>()
        //Players.slice(Players.weight).selectAll()
    }
}