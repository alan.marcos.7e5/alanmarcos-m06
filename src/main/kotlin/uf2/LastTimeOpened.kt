package uf2

import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import java.time.LocalDate
import java.time.LocalDateTime


/**
 * Fes un programa que cada cop que s'obri, enmagatzemi la data actual (amb hora) a una base de dades,
 * i després imprimeixi les dates en les que s'ha obert. Usa una BD H2 en fitxer.
 * Hecha con DAO
 */

fun main() {

    Database.connect("jdbc:h2:mem:test", "org.h2.Driver")

    transaction {
        // print sql to std-out
        addLogger(StdOutSqlLogger)
        SchemaUtils.create(Hours) //create table in DB
        println("")

        //create new hour
        val hourAcces = Hour.new {
            sequelId = 1
            hour = LocalDateTime.now().toString()
        }

        //read from table
        val hours = Hour.all()
        hours.forEach { println("id: ${it.id} -> hour: ${it.hour}") }
    }


}

object Hours: IntIdTable() {
    val sequelId: Column<Int> = integer("sequel_id").uniqueIndex()
    val hour: Column<String> = varchar("hour", 50)
}

class Hour(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<Hour>(Hours)
    var sequelId by Hours.sequelId
    var hour     by Hours.hour
}
