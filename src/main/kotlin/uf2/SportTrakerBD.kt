package uf2

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.transactions.transaction
import uf1.ejerciciosXml.Training
import java.util.Scanner

/**
 * Torna a fer l'exercici SportTraker, però aquest cop registra les dades en una BD. Usa una BD H2 en fitxer.
 * Hecha con DSL
 */

fun main() {
    databaseOperations()
}

fun databaseOperations() {
    Database.connect("jdbc:h2:mem:test", "org.h2.Driver")

    transaction {
        addLogger(StdOutSqlLogger)
        createTable()

        val scanner = Scanner(System.`in`)
        var idToPass = 1
        while (true) {
            println("Entry sport and duration:")
            val userInputSport = scanner.nextLine()
            if (userInputSport == "-1") break
            val userInputDuration = scanner.nextLine().toInt()
            val sport: Training = Training(userInputSport, userInputDuration)
            addInTable(Sports, sport, idToPass)
            idToPass++
        }
        println("Entry sport to see hours:")
        val sportName = scanner.nextLine()
        readHoursBySport(Sports,sportName)
        println(readTotalHours(Sports))
    }
}

fun readHoursBySport(sports: Sports, sportName: String) {
    println(sports.select(sports.sport eq sportName).groupBy { sports.sport })
}

fun createTable() {
    SchemaUtils.create (Sports)
}

fun readTotalHours(sports: Sports): Int {
    var totalHours = 0
    //sports.selectAll().forEach { println(it[sports.sport]) }
    sports.selectAll().forEach {
        totalHours += it[sports.hours]
    }
    return totalHours
}

fun addInTable(sports: Sports, sport: Training, idToPass: Int) {
    sports.insert {
        it[sports.id] = idToPass
        it[sports.sport] = sport.sport
        it[sports.hours] = sport.duration
    }

}

object Sports: Table() {
    val id: Column<Int> = integer("id").uniqueIndex()
    val sport: Column<String> = varchar("sport", 50)
    val hours: Column<Int> = integer("hours")
}

