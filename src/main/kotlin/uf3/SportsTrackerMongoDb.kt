package uf3

import com.mongodb.ConnectionString
import com.mongodb.MongoClientSettings
import com.mongodb.ServerApi
import com.mongodb.ServerApiVersion
import com.mongodb.client.MongoClient
import com.mongodb.client.MongoClients
import com.mongodb.client.MongoCollection
import com.mongodb.client.MongoDatabase
import com.mongodb.client.model.Accumulators
import com.mongodb.client.model.Aggregates
import com.mongodb.client.model.Filters.eq
import org.bson.codecs.configuration.CodecRegistries
import org.bson.codecs.pojo.PojoCodecProvider
import org.bson.types.ObjectId
import java.util.*


/**
 * All properties of the data class must be initialized and be var.
 */
data class Sport(
    var _id: ObjectId = ObjectId.get(),
    var sport: String ="",
    var hours: Int=0,
    var owner_id: String = "")

fun main() {
    //Client
    val pojoCodecProvider = PojoCodecProvider.builder().automatic(true).build()
    val pojoCodecRegistry = CodecRegistries.fromRegistries(
        MongoClientSettings.getDefaultCodecRegistry(),
        CodecRegistries.fromProviders(pojoCodecProvider)
    )
    val connectionString =
        ConnectionString("mongodb+srv://alan:ITB2021316@cluster0.1dsw7w8.mongodb.net/?retryWrites=true&w=majority")
    val settings: MongoClientSettings = MongoClientSettings.builder()
        .applyConnectionString(connectionString)
        .serverApi(
            ServerApi.builder()
                .version(ServerApiVersion.V1)
                .build()
        )
        .build()
    val mongoClient: MongoClient = MongoClients.create(settings)

    //Collection
    val database: MongoDatabase = mongoClient.getDatabase("SportTracker")
        .withCodecRegistry(pojoCodecRegistry);
    val collection: MongoCollection<Sport> = database.getCollection("Sport", Sport::class.java)

    val scanner = Scanner(System.`in`)
    while (true) {
        showMenu()
        when (scanner.nextLine()) {
            "1" -> addSport(scanner, collection)
            "2" -> readAllSports(collection)
            "3" -> findSportByName(scanner, collection)
            "4" -> findSportHours(scanner, collection)
            "5" -> break
            else -> {
                println("Not valid selection")
                break
            }
        }

    }



}

fun findSportHours(scanner: Scanner, collection: MongoCollection<Sport>) {
    //TODO
    println("Entry sport name to see total hours")
    val sportName = scanner.nextLine()
    val sportTotalHours = collection.aggregate(
        listOf(
            Aggregates.group("\$sport",
                Accumulators.sum("sportHours", "\$hours"))
        )
    ).forEach { println(it.sport) }

    println(sportTotalHours)
}

private fun findSportByName(scanner: Scanner, collection: MongoCollection<Sport>) {
    println("Entry sport name to see hours:")
    val sportName = scanner.nextLine()
    val sportResult = collection.find(eq("sport", sportName)).singleOrNull()?.hours?.toString() ?: "No está registrado"
    println(sportResult)
}

private fun readAllSports(collection: MongoCollection<Sport>) {
    //read
    val sports = collection.find().toList()
    println(sports)
}

private fun addSport(scanner: Scanner, collection: MongoCollection<Sport>) {
    println("Entry sport: ")
    val userInputSport = scanner.nextLine()
    println("Entry duration: ")
    val userInputDuration = scanner.nextLine().toInt()
    val sport: Sport = Sport(sport = userInputSport, hours = userInputDuration)
    //write
    collection.insertOne(sport)
}

private fun showMenu() {
    println("What would you do?\n" +
            "1-Add sport.\n" +
            "2-Read log.\n" +
            "3-View sport.\n" +
            "4-Quit.\n")
}
