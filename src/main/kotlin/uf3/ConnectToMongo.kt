package uf3

import com.mongodb.ConnectionString
import com.mongodb.MongoClientSettings
import com.mongodb.MongoClientSettings.getDefaultCodecRegistry
import com.mongodb.ServerApi
import com.mongodb.ServerApiVersion
import com.mongodb.client.MongoClient
import com.mongodb.client.MongoClients
import com.mongodb.client.MongoCollection
import com.mongodb.client.MongoDatabase
import com.mongodb.client.model.Filters
import org.bson.codecs.configuration.CodecRegistries.fromProviders
import org.bson.codecs.configuration.CodecRegistries.fromRegistries
import org.bson.codecs.pojo.PojoCodecProvider
import org.bson.types.ObjectId

/**
 * Fes un programa de consola que permeti a un esportista enregistrar els diferents entrenaments que fa en un xml.

El programa ha de preguntar a l'usuari (per consola) quin esport ha fet i la duració.
La informació s'ha de guardar en una DB MongoDB.
Imprimeix els log de totes les dades introduïdes.
Imprimirà la duració total registrada i la total per cada esport (els càlculs els ha de fer la BD)
El programa es tancarà (per enregistrar un altre entrada ha de tornar a obrir el programa)
 */


data class Item(
    var _id: ObjectId = ObjectId.get(),
    var complete: Boolean = false,
    var summary: String = "",
    var owner_id: String = "")

fun main() {
    //Client
    val pojoCodecProvider = PojoCodecProvider.builder().automatic(true).build()
    val pojoCodecRegistry = fromRegistries(getDefaultCodecRegistry(), fromProviders(pojoCodecProvider))
    val connectionString =
        ConnectionString("mongodb+srv://alan:ITB2021316@cluster0.ii9lmbe.mongodb.net/?retryWrites=true&w=majority")
    val settings: MongoClientSettings = MongoClientSettings.builder()
        .applyConnectionString(connectionString)
        .serverApi(
            ServerApi.builder()
                .version(ServerApiVersion.V1)
                .build()
        )
        .build()
    val mongoClient: MongoClient = MongoClients.create(settings)

    //Collection
    val database: MongoDatabase = mongoClient.getDatabase("SportTracker")
        .withCodecRegistry(pojoCodecRegistry);
    val collection: MongoCollection<Item> = database.getCollection("Item", Item::class.java)
    println(collection)

    //Add to DB
    val item = Item(summary = "Something3", complete = true)
    //collection.insertOne(item)


    //Read from DB
    val items = collection.find().toList()
    val notCompleted = collection.find(Filters.eq("complete", false)).toList()
    //val completed = collection.find(Filters.eq("complete", true)).toList()[0].summary

    println(items)
    println(notCompleted)
}