package uf3

import io.realm.kotlin.Realm
import io.realm.kotlin.ext.query
import io.realm.kotlin.log.LogLevel
import io.realm.kotlin.mongodb.App
import io.realm.kotlin.mongodb.AppConfiguration
import io.realm.kotlin.mongodb.Credentials
import io.realm.kotlin.mongodb.subscriptions
import io.realm.kotlin.mongodb.sync.SyncConfiguration
import java.util.*

suspend fun main() {
    val realmApp = App.create(
        AppConfiguration.Builder("application-0-pcdby") //app id from app services in atlas.
        .log(LogLevel.ALL)
        .build())

    val scanner = Scanner(System.`in`)
    println("User:")
    val userName = scanner.nextLine()
    println("Password:")
    val userPassword = scanner.nextLine()

    //realmApp.emailPasswordAuth.registerUser(userName, userPassword) //register user.

    val creds = Credentials.emailPassword(userName, userPassword)
    realmApp.login(creds) //need login to register successfully. THis line is principally to log in the user.
    val user = realmApp.currentUser!!
    println(user)

    //remote config
    val config = SyncConfiguration.Builder(user, setOf(SportRealm::class))
        .initialSubscriptions { realm ->
            add(
                realm.query<SportRealm>(),
                "All Sports"
            )
            add(
                realm.query<SportRealm>("owner_id == $0", realmApp.currentUser!!.id),
                "User's Sports"
            )
        }
        .waitForInitialRemoteData()
        .build()
    println("Realm Path: ${config.path}")
    val realm = Realm.open(config)
    realm.subscriptions.waitForSynchronization()

    //insert data

    println("sport:")
    val userSport = scanner.nextLine()
    println("hours:")
    val userHours = scanner.nextLine().toInt()

    realm.writeBlocking {
        val sport = SportRealm(
            sport = userSport,
            hours = userHours,
            owner_id = user.id
        )
        copyToRealm(sport)
    }
}