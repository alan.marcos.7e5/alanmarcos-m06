package uf3

import io.realm.kotlin.Realm
import io.realm.kotlin.RealmConfiguration
import io.realm.kotlin.ext.query
import io.realm.kotlin.types.ObjectId
import io.realm.kotlin.types.RealmObject
import io.realm.kotlin.types.annotations.PrimaryKey
import kotlinx.coroutines.flow.collect
import java.util.Scanner
import kotlin.reflect.KClass
import kotlin.reflect.jvm.internal.impl.resolve.constants.IntValue


open class SportRealm(
    @PrimaryKey
    var _id: ObjectId = ObjectId.create(),
    var sport: String = "",
    var hours: Int = 0,
    var owner_id: String = ""
) : RealmObject {
    // Declaring empty contructor
    constructor() : this(owner_id = "") {}
    //var doAfter: RealmList<Item>? = realmListOf()
    override fun toString() = "SportRealm($_id, $sport, $hours)"
}

suspend fun main() {

    //local config
    val config = RealmConfiguration.Builder(setOf(SportRealm::class))
        .deleteRealmIfMigrationNeeded()
        // .directory("customPath")
        .build()

    println("Realm Path: ${config.path}") //the path of the file that has the database.
    val realm = Realm.open(config)

    val scanner = Scanner(System.`in`)
    println("Sport:")
    val userSport = scanner.nextLine()
    println("Hours:")
    val userHours = scanner.nextLine().toInt()

    //insert data
    realm.writeBlocking {
        val sport = SportRealm().apply {
            sport = userSport
            hours = userHours
            // doAfter = query<Item>().find().take(2).toRealmList()
        }
        copyToRealm(sport)
    }
    //Read all data
    val allSPorts = realm.query<SportRealm>().find()
    allSPorts.forEach { println(it) }

    //Read data and sum all hours
    val totalHours = realm.query<SportRealm>().sum("hours", Int::class).find()
    println(totalHours)

    realm.query<SportRealm>().distinct("sport")
    //Read data for one sport
    val sports = realm.query<SportRealm>("sport = '$userSport'").find()
    sports.forEach { println(it) }

    //Read hours for each sport
    realm.query<SportRealm>().distinct("sport").find().forEach {
        println("${it.sport}: " + realm.query<SportRealm>("sport = '${it.sport}'").sum("hours", Int::class).find())
    }

    //Read data with FLow. It`s like an Observable
    /*realm.query<SportRealm>().find().asFlow().collect{ it ->
        println(it.list.map{it})
        println()
    }
    println("Nunca llega a este punto, la query anterior es como un bucle")*/
    //realm.close()

}