package uf1.ejerciciosXml

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XML
import java.util.Scanner
import kotlin.io.path.Path
import kotlin.io.path.readText

/**
 * Fes un programa que llegeixi el següent xml amb informació de restaurant i
 * l'imprimeixi per pantalla els restaurants segons el tipus que indica l'usuari.

    Quin tipus de restaurant vols?
    > tapes
    Bar Pasqual - Avinguda Diagonal 232 - Joan Martí
    Tapitapes - Balmes 2 - Gustaf Michelin
 */

@Serializable
@SerialName("restaurants")
data class Restaurants(val restaurant: List<Restaurant>)

fun main() {
    val scanner = Scanner(System.`in`)
    val xml = Path("ficheros/restaurants.xml").readText()
    val restaurants: Restaurants = XML.decodeFromString(xml) //es necesario especificar el tipo
    println("Quin tipus de restaurant vols?")
    val inputType = scanner.nextLine()
    println(restaurants.restaurant.filter { it.type == inputType})
}