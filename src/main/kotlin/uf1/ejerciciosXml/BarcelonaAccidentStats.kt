package uf1.ejerciciosXml

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.csv.Csv
import kotlin.io.path.Path
import kotlin.io.path.readText

/**
 * Fes un programa que llegeixi els fitxers que contiene la carpeta ficheros/accidentsStats per imprimir
 * les següents dades en el següent format (ver consigna en classroom)
 */


/**
 * "Numero_expedient","Codi_districte","Nom_districte","Codi_barri","Nom_barri","Codi_carrer","Nom_carrer","Num_postal ","Descripcio_dia_setmana","NK_Any","Mes_any","Nom_mes","Dia_mes","Hora_dia","Descripcio_torn","Descripcio_causa_mediata","Coordenada_UTM_X_ED50","Coordenada_UTM_Y_ED50","Longitud_WGS84","Latitud_WGS84"
 */

@Serializable
data class AccidentsCauses(@SerialName("Numero_expedient") val numExpedient: String )

@Serializable
data class Accidents(@SerialName("Numero_morts") val numDeaths: String, @SerialName("Numero_victimes") val numVictims: String )

fun main() {
    //val pathCauses = Path("ficheros/accidentsStats/2021_accidents_causes_gu_bcn_.csv").readText()
    val pathAccidents = Path("ficheros/accidentsStats/2021_accidents_gu_bcn.csv").readText()

    //val decodedAccidents = CSVFormat.decodeFromString(Accidents.serializer(),pathAccidents)

    val csv = Csv { ignoreUnknownColumns=true; hasHeaderRecord=true;ignoreEmptyLines=true}
    val decodedAccidents = csv.decodeFromString(ListSerializer(Accidents.serializer()),pathAccidents)
    println("Total: ${decodedAccidents.size}")
    println("Víctimas: ${decodedAccidents.sumOf { it.numVictims.toInt()}}")


}