package uf1.ejerciciosXml

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XML
import java.nio.file.Path
import java.nio.file.StandardOpenOption
import java.util.*
import kotlin.io.path.Path
import kotlin.io.path.readText
import kotlin.io.path.writeText


/**
 *Fes un programa de consola que permeti a un esportista
 * enregistrar els diferents entrenaments que fa en un xml.

El programa ha de preguntar a l'usuari (per consola) quin esport ha fet i la duració.
La informació s'ha d'anar afegint a un xml.
Un cop introdueixi les dades s'imprimirà la duració total registrada i la total per cada esport
El programa es tancarà (per enregistrar un altre entrada ha de tornar a obrir el programa)
 */

@Serializable
@SerialName("training")
data class Training(val sport: String, var duration: Int)

@Serializable
@SerialName("trainings")
data class Trainings(val listTrainings: List<Training>)

@Serializable
@SerialName("athlete")
data class Athlete(val trainings: Trainings)

fun main() {
    startSportTracker()
}

private fun menuSportTracker(){
    println("1. Add new training session\n" +
            "2. Exit")
}

private fun startSportTracker() {
    val scanner = Scanner(System.`in`)
    while (true) {
        menuSportTracker()
        var userMenuSelection = scanner.nextLine()
        when(userMenuSelection){
            "1" -> newTrainingAdd(scanner)
            "2" -> break
        }
        userMenuSelection = scanner.nextLine()
    }
}

private fun newTrainingAdd(scanner: Scanner) {
    val listTrainings = mutableListOf<Training>()
    val newTraining = Training(scanner.next(), scanner.nextInt())
    listTrainings += newTraining

    val path = Path("ficheros/sportTraker.txt")
    if (checkPath(path)) {
        val athlete = readAthleteFromFile(path)
        athlete.trainings.listTrainings.forEach { listTrainings += it }
        writeInPath(path, createAthleteXml(listTrainings))
    }else {
        createPath(path, createAthleteXml(listTrainings))
    }

    showHours(listTrainings)
}

private fun showHours(listTrainings: MutableList<Training>) {
    val mapOfSports = mutableMapOf<String, Int>()
    var totalHours = 0
    listTrainings.forEach {
        totalHours += it.duration
        println("${it.sport} = ${it.duration}")
        /*if (it.sport in mapOfSports) {
            mapOfSports[it.sport] = mapOfSports[it.sport]!! + it.duration
        }else mapOfSports[it.sport] = it.duration*/
    }
    println(totalHours)
}

private fun createAthleteXml(listTrainings: List<Training>): String{
    val newAthlete = createAthlete(listTrainings)
    return XML.encodeToString(newAthlete)
}

private fun readAthleteFromFile(path: Path): Athlete {
    val xml = path.readText()
    return XML.decodeFromString(xml)
}

private fun createAthlete(listTrainings: List<Training>) = Athlete(Trainings(listTrainings))

fun checkPath(path: Path) = path.toFile().exists()

fun createPath(path: Path, xmlData: String) {
    path.writeText(xmlData, options = arrayOf(StandardOpenOption.CREATE))
}

fun writeInPath(path: Path, xmlData: String) {
    path.writeText(xmlData, options = arrayOf(StandardOpenOption.WRITE))
}

fun appendInPath(path: Path, xmlData: String) { //left verify
    path.writeText(xmlData, options = arrayOf(StandardOpenOption.APPEND))
}






