package uf1.ejerciciosXml

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XML
import nl.adaptivity.xmlutil.serialization.XmlElement
import java.util.*
import kotlin.io.path.Path
import kotlin.io.path.readText

/**
 * Donat el següent xml de receptes fes un programa que
 * pregunti un nom a l'usuari i que imprimeixi les receptes que tenen aquest ingredient ordenades per dificultat.
 */

@Serializable
@SerialName("ingredient")
data class Ingredient(val ammount: String, val unit: String, val name: String)

@Serializable
@SerialName("ingredients")
data class Ingredients(val listIngredients: List<Ingredient>)

@Serializable
@SerialName("recipe")
data class Recipe(val dificulty: String, @XmlElement(true) val name: String, @XmlElement(true) val ingredients: Ingredients)

@Serializable
@SerialName("recipes")
data class Recipes(val listRecipes: List<Recipe>)


fun main() {
    /*val ingredient = Ingredient("2","gr", "salsa")
    val ingredients = Ingredients(List(2){ingredient})
    val recipe = Recipe("8","canelones", ingredients)
    val recipes = Recipes(List(2){recipe})
    println(ingredient)
    println(recipe)
    println(recipes)

    println(XML.Companion.encodeToString(ingredient))
    println(XML.Companion.encodeToString(recipe))
    println(XML.Companion.encodeToString(recipes))*/

    val xml = Path("ficheros/receptes.xml").readText()
    val recipes: Recipes = XML.decodeFromString(xml)
    println(recipes)
    val scanner = Scanner(System.`in`)
    val userInputIngredient = scanner.nextLine()
    val listRecipesFiltered = mutableListOf<Recipe>()

    recipes.listRecipes.forEach { recipe ->
        //recipe.ingredients.listIngredients.filter { it.name == userInputIngredient }
        recipe.ingredients.listIngredients.forEach {
            if (it.name == userInputIngredient) listRecipesFiltered += recipe
        }
    }
    println(listRecipesFiltered.sortedByDescending { it.dificulty })
}
