package uf1.ejerciciosXml

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XML
import nl.adaptivity.xmlutil.serialization.XmlElement
import kotlin.io.path.Path
import kotlin.io.path.readText

@Serializable
@SerialName("restaurant")
data class Restaurant(val type: String, @XmlElement(true) val name: String, @XmlElement(true) val address: String, @XmlElement(true) val owner: String)


fun main() {
    /*val r = Restaurant("dsadsa", "dsaasd", "dsadsa", "dsaads")
    println(XML.encodeToString(r))*/
    val xml = Path("ficheros/restaurant.xml").readText()
    val restaurant : Restaurant = XML.decodeFromString(xml)
    println(restaurant)
}
