import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.7.21"
    kotlin("plugin.serialization") version "1.7.21"
    id("io.realm.kotlin") version "1.5.0"
}

group = "org.example"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    maven ( "https://jitpack.io" )
}

val exposedVersion: String by project
dependencies {
    testImplementation(kotlin("test"))
    implementation("io.github.pdvrieze.xmlutil:core-jvm:0.84.2")
    implementation("io.github.pdvrieze.xmlutil:serialization-jvm:0.84.2")
    implementation("app.softwork:kotlinx-serialization-csv:0.0.6")
    implementation("app.softwork:kotlinx-serialization-flf:0.0.6")
    // Kotlin Serialization CSV
    implementation("de.brudaswen.kotlinx.serialization:kotlinx-serialization-csv:2.0.0")
    //Telegram bot
    implementation ("io.github.kotlin-telegram-bot.kotlin-telegram-bot:telegram:6.0.7")
    //h2
    implementation("com.h2database:h2:2.1.214")
    //postgreSQL
    implementation("org.postgresql:postgresql:42.5.0")
    //exposed orm
    implementation("org.jetbrains.exposed:exposed-core:$exposedVersion")
    implementation("org.jetbrains.exposed:exposed-dao:$exposedVersion")
    implementation("org.jetbrains.exposed:exposed-jdbc:$exposedVersion")
    implementation("org.slf4j:slf4j-simple:2.0.3")
    //MongoDB
    implementation("org.mongodb:mongodb-driver-sync:4.7.1")
    //Realm Local: -Coroutines and -Realm
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.6.0")
    implementation("io.realm.kotlin:library-sync:1.5.0")

}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}